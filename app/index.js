import configureStore from './store/configureStore'
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, hashHistory ,Link,browserHistory} from 'react-router';
import { syncHistoryWithStore,push } from 'react-router-redux';
import io from 'socket.io-client'
import routes from './routes'
import {setUserList,setVmMapping,setState,setAppList,setAuthentication,createUserResult} from './actions/actions'

import 'react-mdl/extra/material.css';
import 'react-mdl/extra/material.js';
import mori from 'mori'
import * as path from './path';
import {createHistory} from 'history'
let socket=io(path.server);
socket.on('initialData',(initialData)=>{

  if(initialData.entries!=undefined){
    store.dispatch(setState(initialData.entries))
    // console.log(' ---- initialData --- ',initialData)
  }
  if(initialData.vm_mapping!=undefined){
    store.dispatch(setVmMapping(initialData.vm_mapping))
    // console.log(' ---- initialData --- ',initialData)
  }

  if(initialData.appList != undefined){
    store.dispatch(setAppList(initialData.appList));

  }
  if(initialData.userList != undefined){
    store.dispatch(setUserList(initialData.userList))
  }
  });
socket.on('disconnect',()=>{
  console.log('disconnected client')
})
socket.on('error',() =>{
  console.log('error ------------------ ')
})
socket.on('authenticationResult',function(obj){
  var parsedObj=obj;

  if(parsedObj.isLoggedIn===false){
    store.dispatch(setAuthentication(parsedObj))
  }else{

    store.dispatch(setAuthentication(parsedObj));

    pathChange(history,'dashboard')

  }
})
socket.on('create_user_account_result',function(result : boolean){
  if(result.result === true){
    console.log('in create user result ',result)
    store.dispatch(createUserResult(result))
  }else{
    //user exist
    store.dispatch(createUserResult(result))
    console.log('in create user result ',result)
  }
})
export function auth(){

  if(mori.toJs(store.getState().reducers).isLoggedIn === true){


  }else{

    pathChange(history,'login')
  }
}
const store=configureStore(undefined,socket);
const history=syncHistoryWithStore(hashHistory,store);

//Debugg--
store.subscribe(()=>{
  //console.log('Debugging : Store -- ',store.getState())
})
//
render(
  <Provider store={store}>
    <Router history={history} routes={routes} />
  </Provider>,
  document.getElementById('root')
);

function pathChange(history,pathName) {
  history.push(pathName)
}

/*const socket = io.connect('http://192.168.223.34:8090/', { query: "macId=02:42:a9:07:5e:32" });
socket.on('state', state =>{
  console.log('vector  state', state.entries)
  if(state.entries!=undefined){
    store.dispatch(setState(state.entries))
    socket.close()
  }
})
socket.on('disconnect',()=>{
  console.log('disconnected client')
})
socket.on('error',() =>{
      console.log('error ------------------ ')
})*/
