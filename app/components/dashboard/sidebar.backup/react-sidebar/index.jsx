// @flow
import React,{Component} from 'react';
import Sidebar from './renderer/sidebar';
import SidebarContent from './renderer/sidebar_content';
import Odduu from '../../../renderer/main_body/odduu'
import MaterialTitlePanel from './renderer/material_title_panel';
const mql = window.matchMedia(`(min-width: 800px)`);
import BodyRender from './renderer/body';
const styles = {
  contentHeaderMenuLink: {
    textDecoration: 'none',
    color: 'white',
    padding: 8,
  },
  content: {
    padding: '16px',
  },

};
export default class SidebarRender2 extends Component{
  constructor(props) {
    super(props);

    this.state = {
      mql: mql,
      sidebarDocked: props.docked,
      sidebarOpen: props.open
    }

    this.mediaQueryChanged = this.mediaQueryChanged.bind(this);
    this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this);

    this.componentWillMount=this.componentWillMount.bind(this);
    this.componentWillUnmount=this.componentWillUnmount.bind(this);
    this.toggleOpen=this.toggleOpen.bind(this);
  }
  toggleOpen(ev) {
    this.setState({sidebarOpen: !this.state.open});

    if (ev) {

      ev.preventDefault();
    }
  }

  onSetSidebarOpen(open) {
    this.setState({sidebarOpen: open});
  }

  componentWillMount() {
    mql.addListener(this.mediaQueryChanged);
    this.setState({mql: mql, sidebarDocked: mql.matches});
  }

  componentWillUnmount() {
    this.state.mql.removeListener(this.mediaQueryChanged);
  }

  mediaQueryChanged (){
    this.setState({sidebarDocked: this.state.mql.matches});
  }
  render() {
    var sidebarContent = <SidebarContent />
    var sidebarProps = {
      sidebar: this.state.sidebarOpen,
      docked: this.state.sidebarDocked,
      onSetOpen: this.onSetSidebarOpen
    };

    var co=this.props.children;
    const contentHeader = (
      <span>
        {!this.state.sidebarDocked &&
         <a onClick={this.toggleOpen} href="#" style={styles.contentHeaderMenuLink}>=</a>}
        <span> ODDUU Dashboard </span>
      </span>
    );

    return (


      <Sidebar sidebar={sidebarContent}
               open={this.state.sidebarOpen}
               docked={this.state.sidebarDocked}
               onSetOpen={this.onSetSidebarOpen}>

        <MaterialTitlePanel title={contentHeader}>
          <BodyRender>{this.props.children}</BodyRender>

        </MaterialTitlePanel>

      </Sidebar>


    )
  }
}
