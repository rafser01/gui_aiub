// @flow
import {StyleSheet,css} from 'aphrodite'
import React from 'react';
import {Grid,Cell}  from 'react-mdl';
const styles = StyleSheet.create({
  root: {
    fontFamily: '"HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif',
    fontWeight: 300,
  },
  header: {
    backgroundColor: '#03a9f4',
    color: 'white',
    fontSize: '1.5em',
    display: 'block'

  },
  notification:{
    fontSize:'14px',
    'z-index': 10,
    position: 'absolute',
    right: 0,


  },
  title:{
    position:'absolute',
    left:'5%',
  }
});

const MaterialTitlePanel = (props : MaterialTitleProps) => {
  const rootStyle = props.style ? {...styles.root, ...props.style} : css(styles.root);
  //const notificationStyle=props.notificationStyle ? {...props.notificationStyle} : css(styles.notification);
  return (
    <div className={rootStyle }>
        <Grid className={css(styles.header)}>
            <Cell col={3} >
              <div className={css(styles.title)}>{props.title} </div>
            </Cell>

            <Cell col={3} >
              <div className={css(styles.notification)}>
                {props.notification}
                {props.message}
              </div>
            </Cell>
        </Grid>

      {props.children}

    </div>
  );
};

// MaterialTitlePanel.propTypes = {
//   style: React.PropTypes.object,
//   title: React.PropTypes.oneOfType([
//     React.PropTypes.string,
//     React.PropTypes.object,
//   ]),
//   children: React.PropTypes.object,
// };
let MaterialTitleProps={
  style:{},
  title : String || {},
  children : {}
}

export default MaterialTitlePanel;
