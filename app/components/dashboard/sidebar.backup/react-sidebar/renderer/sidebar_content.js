import React from 'react';
import {Link} from 'react-router';
import MaterialTitlePanel from './material_title_panel';
import {Grid,Coll} from 'react-mdl';
const styles = {
  sidebar: {
    width: 100,
    height: '',
  },
  sidebarLink: {
    display: 'block',
    padding: '16px 0px',
    color: '#757575',
    textDecoration: 'none',
  },
  divider: {
    
    height: 1,
    backgroundColor: '#757575',
  },
  content: {
    padding: '16px',
    height: '100%',
    backgroundColor: 'white',
  },
};

const SidebarContent = (props) => {
  const style = props.style ? {...styles.sidebar, ...props.style} : styles.sidebar;

  const links = [];
  let menu=[{'menu':'ODDUU', 'link':'odduu'},{'menu':'VM_WIN','link': 'vm_win'}];

  for (let ind = 0; ind <menu.length; ind++) {
    links.push(
      <Link key={ind} to={'dashboard/'+menu[ind].link} style={styles.sidebarLink}>{menu[ind].menu}</Link>
    )
  }

  return (
    <MaterialTitlePanel title={props.title} style={style}>
      <div style={styles.content}>

        <div style={styles.divider} ></div>
        {links}
      </div>

    </MaterialTitlePanel>

  );
};

SidebarContent.propTypes = {
  style: React.PropTypes.object,
};

export default SidebarContent;
