// @flow
import Notification from '../models/notification';
export function setState(state){
  return {
    type:'SET_ENTRIES',
    entries: state
    }
}
export function setToggleActions(val){
	return {type:'TOGGLE',
			val
			}
}
export function isOpenDialog(val){
  return {type:'IS_OPEN_DIALOG',
  value:val
};
}
export function isOpenMessage(val){
  return {type:'IS_OPEN_MESSAGE',
  value:val
}
};
export function isOpenAppList(val){

  return {type:'IS_OPEN_APP_LIST',
  value:val
}
}
export function getInstalledApps(mac,index){
  return { type: 'INSTALLED_APPS',
            mac:mac,
            index:index
}
}

export function addNewNotification(notification : Notification){
  return {
    type: 'NEW_NOTIFICATION',
    notification : notification
  }
}
export function setAppList(appList){
  return {type:'SET_APP_LIST',
          apps:appList
  }
}
export function bulkInstall(items,system,macList){
  return {
    type:'BULK_INSTALL',
    items:items,
    osType:system,
    macList:macList
  }
}
export function bulkUnInstall(items,system,macList){
  return {
    type:'BULK_UNINSTALL',
    items:items,
    osType:system,
    macList:macList
  }
}
//choose mac _ips to hit commands
export function chooseMacIPS(macList,system){
  return {
    type:'MAC_IP_LIST',
    macList:macList,
    osType:system
  }
}

export function shutdown(mac,type){
  return {
    type:'SHUTDOWN',
    mac:mac,
    machine_type:type,
  }
}
export function reboot(mac,type){
  return {
    type:'REBOOT',
    mac:mac,
    machine_type:type,
  }
}
export function setSelectedLabNumber(labNumber,type){
  return {
    type : 'SET_SELECTED_LAB_NUMBER',
    labNumber: labNumber,
    machine_type : type
  }
}
export function setAuthentication(obj){
  return {
    type:'SET_AUTHENTICATION',
    data:obj
  }
}
export function submitLogin(logInObj){
  return {
    type : 'LOGIN',
    data:logInObj
  }
}

export function defaultValue(check,value) {
  return (typeof check !== 'undefined' ? check : value)
}
export function createUser(name,id,password,role){
  var data={
    name:name,
    userId:id,
    password:password,
    role:role,
  }
  return {
    type : 'CREATE_USER',
    data:data,
  }
}
export function createUserResult(result) {
  return {
    type: 'CREATE_USER_RESULT',
    data:result
  }
}
export function userExistChange(value){
  return {
    type:'USER_EXIST_CHANGE',
    data:value
  }
}
export function logout(){
  return {
    type:'LOGOUT'
  }
}
export function singleVmMapping(clientMac,vmWinMac,vmLinMac,vmIp) {
  console.log(' Single VM Mapping ',clientMac,'win ',vmWinMac,'lin',vmLinMac,'vmIp',vmIp);

  if(vmLinMac === ''){
    return {
      type : 'CLIENT_WINVM_MAPPING',
      clientMac:clientMac,
      vmWinMac:vmWinMac,
      vmWinIP:vmIp
    }
  }else{
    return {
      type : 'CLIENT_LINVM_MAPPING',
      clientMac:clientMac,
      vmLinMac:vmLinMac,
      vmLinuxIP:vmIp,
    }
  }
}
export function setVmMapping(data){
  return {
    type:'VM_MAPPING',
    data:data
  }
}
export function submitLinuxApp(values){
  return {
    type:'LINUX_APP',
    data:values
  }
}
export function setUserList(userList){
  return {
    type:'SET_USER_LIST',
    userList:userList
  }
}
export function executeClientCmd(cmd,macList) {

  return {
    type:'CLIENT_CMD',
    cmd:cmd,
    macList:macList
  }
}
