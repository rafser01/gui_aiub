// @flow
export default class Notification {
  constructor(id : number ,title : String ,details : String){
    this.id=id;
    this.title=title;
    this.details=details;
  }
}
